console.log('Hello World!');

let num1 = parseInt(prompt('Provide a first number'));
let num2 = parseInt(prompt('Provide a second number'));

let sum = num1 + num2;

if (sum < 10 && sum > 0) {

	console.warn('The sum of the two numbers is: ' + sum);

} else if (sum >= 10 && sum <= 19) {

	difference = (num1 - num2);

	alert('The difference of the two numbers is: ' + difference);

} else if (sum >= 20 && sum <= 29) {

	product = num1 * num2;

	alert('The product of the two numbers is: ' + product);

} else if (sum >= 39) {

	quotient = num1 / num2;

	alert('The quotient of the two numbers is: ' + quotient);

} else {

	alert('Kindly provide valid integer input.');
}




// Logical OR and AND Operators

let name = prompt('Your name is ');
let age = parseInt(prompt('How long have you lived on Earth? '));

if ((name.length !== 0 ) && (age > 0)) {

	alert(name + ' is ' + age + ' years of age');

} else {

	alert('Are you a time traveler?');
}


// Functions

function isLegalAge() {

	if (age >= 18) {

		alert('Congratulations! You are now allowed to party.');

	} else if (age >= 21) {

		alert('Cheers! You are now part of the adult society.');

	} else if (age >= 65) {

		alert('We thank you for your contribution to society! You can now retire.');

	} else {

		alert('Are you sure you\'re not an alien?');

	}
}

isLegalAge();


// Try Catch Finally

	function showAgeBracket() {

		try {
			console.fog(isLegalAge);
		}

		catch(error) {
			// handle the error
			console.warn(error.message);
		}

		finally {
			// regardless of the result, this will execute
			alert('Age is just a number, live how you want to live!')
		}
	}

	showAgeBracket();